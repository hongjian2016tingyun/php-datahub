<?php
namespace Triple\Datahub;

class Config
{
    public static $mqhost = '127.0.0.1';
    public static $port = '5672';
    public static $user = 'gvr_datahub_station';
    public static $passwd = 'gvr_datahub_passwd';
    public static $vhost = '/';
}


