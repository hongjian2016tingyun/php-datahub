<?php
//$config = require "./Config.php";r

//require_once "./vendor/autoload.php";
namespace Triple\Datahub;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class MqDataCenter
{
    //public $visEventCallback;
    public static function buildChannel(){
        $connection = new AMQPStreamConnection(Config::$mqhost, Config::$port,
            Config::$user,  Config::$passwd, Config::$vhost);
        $channel = $connection->channel();
        return $channel;
    }

    public static function syncGetCpassInfo($exchange,$routtingKey){
        $cpassInfo = new CpassInfo();
        $callback = function ($msg) use($cpassInfo) {
            echo 'rev msg',$msg,"\n";
            $obj = json_decode($msg,true);
            $addinfo = $obj['AddInfo'];
            $cpassInfo->setGunId($addinfo['GunID']);
            $cpassInfo->setState($addinfo['State']);
            $cpassInfo->setTrack1($addinfo['VisID']);
            $cpassInfo->setted = true;

        };
        $channel = self::listener($exchange,$routtingKey,$callback);

        while(!$cpassInfo->setted) {
            $channel->wait();
        }
        $connection =  $channel->getConnection();
        $channel->close();
        $connection->close();
        return $cpassInfo;
    }

    public static function listener($exchange,$routtingKey,$callbackfunc){
        $channel = MqDataCenter::buildChannel();
        $queue = $exchange."_$routtingKey";
        $channel->queue_declare($queue, false, false, false, false);
        $channel->queue_bind($queue,$exchange);
        $mqCallback = function($msg) use ( $callbackfunc ) {
            echo " [x] Received ", $msg->body, "\n";
            call_user_func($callbackfunc,$msg->body);
        };
        $channel->basic_consume($queue, $queue."_consumer", false,
            true, false, false, $mqCallback);
        return $channel;
    }

    public static function publish($jsonStr,$exchange,$routtingKey){
        $channel = MqDataCenter::buildChannel();
        $connection =  $channel->getConnection();
        //发送方其实不需要设置队列， 不过对于持久化有关，建议执行该行
        $channel->queue_declare($exchange.'_queue', false, false, false, false);
        $msg = new AMQPMessage($jsonStr);
        $channel->basic_publish($msg, $exchange, $routtingKey);
        $channel->close();
        $connection->close();
    }



}