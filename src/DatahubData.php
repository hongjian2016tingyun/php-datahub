<?php

namespace Triple\Datahub;

class DatahubData
{
    public $MsgID;
    public $AddInfo;

    public function setMsgId($msgId)
    {
        $this->MsgID = $msgId;
    }

    public function getMsgId()
    {
        return $this->MsgID;
    }

    public function setAddInfo($info)
    {
        $this->AddInfo = $info;
    }

    public function getAddInfo()
    {
        return $this->AddInfo;
    }
}