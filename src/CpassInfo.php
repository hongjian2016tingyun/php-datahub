<?php
//$config = require "./Config.php";

//require_once "./vendor/autoload.php";
namespace Triple\Datahub;


class CpassInfo
{
    public $setted = false;

    public $gunId;
    public $nozNr;
    public $state;
    public $track1;

    public function setGunId($gun)
    {
        $this->gunId= $gun;
    }
    public function getGunId()
    {
        return $this->gunId;
    }
    public function setState($st)
    {
        $this->state= $st;
    }
    public function getState()
    {
        return $this->state;
    }
    public function setTrack1($tr1)
    {
        $this->track1= $tr1;
    }
    public function getTrack1()
    {
        return $this->track1;
    }
}
