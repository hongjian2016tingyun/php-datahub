<?php

namespace Triple\Datahub;

class VisDevice
{
    public $visEventCallback;

    private static $queryCnt;

    public function __construct($host){
       $this->mqHost = $host;
       Config::$mqhost = $host;
    }

    public function queryCpassId($gunId){
        $dhData = new DatahubData();
        VisDevice::$queryCnt += 1;
        $dhData->setMsgId(VisDevice::$queryCnt);
        $addInfo = new AddInfo();
        $addInfo->setGunID($gunId);
        $dhData->setAddInfo($addInfo);
        $objJSon = json_encode($dhData,JSON_FORCE_OBJECT);
        echo  $objJSon,json_last_error(), "\n";
        MqDataCenter::publish($objJSon,'upper_query_vis','query_vis');
        return MqDataCenter::syncGetCpassInfo('upper_query_vis_reply','c_upper_query_vis_reply');
    }
}